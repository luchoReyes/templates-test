## Objective of the Hotfix :fire: :fire_engine: 
[*Describe the main objective of the urgent fix associated with this merge request*]

## Description of tasks performed :pencil: 
[*List the tasks performed in this merge request*]

1. ..
2. ..
3. ..

## Evidence and tests :camera_with_flash: 
[*Provide evidence that the tasks have been completed successfully, such as screenshots, links to online tests, etc.*]
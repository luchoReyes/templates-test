## Objective of the Bugfix :bug: 
[*Describe the main objective of the bugfix associated with this merge request*]

## Description of tasks performed :pencil: 
[*List the tasks performed in this merge request*]

1. ...
2. ...
3. ...

## Evidence and tests :camera_with_flash: 
[*Provide evidence that the bug has been fixed successfully, such as before/after screenshots*]

- [ ] Do you have new tests for this bugfix?
- [ ] Were existing tests modified to accommodate this bugfix?